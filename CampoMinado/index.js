var game = {}
game.campo = new Array(10)
game.visivel = new Array(10)
game.jogar = true
for (var i = 0; i < game.campo.length; i++) { 
    game.campo[i] = new Array(10); 
    game.visivel[i] = new Array(10);
} 

var floorMax = 0;
var currentFloor = 0

for (var i = 0; i < game.campo.length; i++) { 
    for (var j = 0; j < game.campo[0].length; j++) { 
        var random = Math.floor(Math.random()*10)
        if(random > 7){
            game.campo[i][j] = {
                type: 'bomb'
            }
        }
        if(random <= 7){
            game.campo[i][j] = {
                type: 'floor'
            } 
            floorMax++
        }
        game.visivel[i][j] = 'escondido'
    } 
} 

for (var i = 0; i < game.campo.length; i++) { 
    for (var j = 0; j < game.campo[0].length; j++) { 
        
        if( game.campo[i][j].type == 'floor' ){
            var number = 0
            for(var X = -1; X < 2; X++){
                for(var Y = -1; Y < 2; Y++){
                    if(game.campo[X+i]!= undefined && game.campo[X+i][Y+j] != undefined){
                        if(game.campo[X+i][Y+j].type == 'bomb'){
                            number++
                        }
                    }
                }
            }
            game.campo[i][j].number = number
        }
    } 
} 

var canvas = document.getElementById("canvas");
canvas.width = 640
canvas.height = 640
var context = canvas.getContext("2d");


function draw(){
    for (var i = 0; i < game.campo.length; i++) { 
        for (var j = 0; j < game.campo[0].length; j++) { 
            if(game.visivel[i][j] != 'escondido'){
                if(game.campo[i][j].type == "bomb"){
                    context.fillStyle = '#ff0000'
                    context.fillRect(i*64,j*64,64,64);
                }
                if(game.campo[i][j].type == "floor"){
                    context.fillStyle = '#fff'
                    context.fillRect(i*64,j*64,64,64);
                    context.fillStyle = "black";
                    context.font = "30px Arial";
                    context.fillText(game.campo[i][j].number, i*64+24, j*64+44); 
                }
            }else{
                context.fillStyle = '#DDD'
                context.fillRect(i*64,j*64,64,64);
            }
        } 
    } 
}

function procurarZeros(X,Y,get){
    if(get == undefined){
        get = new Array(10)
        for (var i = 0; i < get.length; i++) { 
            get[i] = new Array(10); 
        } 
    }
    get[X][Y] = true
    for (var i = -1; i < 2; i++) { 
        for (var j = -1; j < 2; j++) { 
            if(game.campo[X+i] != undefined && game.campo[X+i][Y+j] != undefined){
                if(get[X+i][Y+j] == undefined){
                    if(game.campo[i+X][j+Y].number == 0){
                        console.log("outros zeros")
                        game.visivel[i+X][j+Y] = 'mostrar'
                        currentFloor++
                        procurarZeros(i+X,j+Y,get)
                    }
                }
            }
        }
    }
    draw()
}

canvas.addEventListener('click', function(e) {
    if(game.jogar == true){
        console.log('click: ' + e.offsetX + '/' + e.offsetY);
        var X = (Math.floor(e.offsetX/64)*64)/64; 
        var Y = (Math.floor(e.offsetY/64)*64)/64;
    
        if(game.campo[X][Y].type != 'bomb'){
            game.visivel[X][Y] = 'mostrar'
            currentFloor++
            if(game.campo[X][Y].number == 0){
                procurarZeros(X,Y)
            }
        }else{
            game.jogar = false
            game.visivel[X][Y] = 'mostrar'
            document.getElementById("EstadoJogo").innerHTML = "Game Over!";
        }

        if(floorMax == currentFloor){
            game.jogar = false
            document.getElementById("EstadoJogo").innerHTML = "Ganhou!";
        }
        draw()
    }
}, false);


draw()

