suspeitos = [
    'Charles B. Abbage',
    'Donald Duck Knuth',
    'Ada L. Ovelace',
    'Alan T. Uring',
    'Ivar J. Acobson',
    'Ras Mus Ler Dorf'
]
locais = [
    'Redmond',
    'Palo Alto',
    'San Francisco',
    'Tokio',
    'Restaurante no Fim do Universo',
    'São Paulo',
    'Cupertino',
    'Helsinki',
    'Maida Vale',
    'Toronto'
]
armas = [
    'Peixeira',
    'DynaTAC 8000X',
    'Trezoitão',
    'Trebuchet',
    'Maça',
    'Gládio'
]

resposta = {
    assassino: {},
    arma: {},
    local: {},
}
probabilidades = {
    suspeitos: {},
    armas: {},
    locais: {},
}


//SETANDO VARIAVEIS
for(var i = 0; i < suspeitos.length; i++){
    probabilidades.suspeitos[i] = 1*i+1
}
for(var i = 0; i < armas.length; i++){
    probabilidades.armas[i] = 1*i+1
}
for(var i = 0; i < locais.length; i++){
    probabilidades.locais[i] = 1*i+1
}

resposta.assassino = suspeitos[Math.floor(Math.random()*suspeitos.length)]
resposta.arma = armas[Math.floor(Math.random()*armas.length)]
resposta.local = locais[Math.floor(Math.random()*locais.length)]


//FUNCOES 
function diminuirProbabilidade(index,quantidade,qual){

    for(var i = index; i < Object.keys(probabilidades[qual]).length; i++){
        probabilidades[qual][i] -= quantidade
    }
}

function aumentarProbabilidade(index,quantidade,qual){
    
    for(var i = index; i < Object.keys(probabilidades[qual]).length; i++){
        probabilidades[qual][i] += quantidade
    }

}

// diminuirProbabilidade(2,4,'suspeitos')
// aumentarProbabilidade(5,10,'suspeitos')
var nacho = true
while(nacho){
    
    var erros = []
    
    var talvezAssassino = null
    var probAssasino = Math.floor(Math.random()*probabilidades.suspeitos[Object.keys(probabilidades.suspeitos-1).length])+1
    for(var i = 0; i < Object.keys(probabilidades.suspeitos).length; i++){
        if(probabilidades.suspeitos[i+1] != undefined){
            if(probabilidades.suspeitos[i] <= probAssasino && probabilidades.suspeitos[i+1] > probAssasino ){
                talvezAssassino = suspeitos[i]
            }
        }else{
            if(probabilidades.suspeitos[i] <= probAssasino){
                talvezAssassino = suspeitos[i]
            }
        }
    }
    
    var talvezLocal = null
    var probLocal = Math.floor(Math.random()*probabilidades.locais[Object.keys(probabilidades.locais).length-1])+1
    for(var i = 0; i < Object.keys(probabilidades.locais).length; i++){
        if(probabilidades.locais[i+1] != undefined){
            if(probabilidades.locais[i] <= probLocal && probabilidades.locais[i+1] > probLocal ){
                talvezLocal = locais[i]
            }
        }else{
            if(probabilidades.locais[i] <= probLocal){
                talvezLocal = armas[i]
            }
        }
    }
    
    
    var talvezArma = null
    var probArmas = Math.floor(Math.random()*probabilidades.armas[Object.keys(probabilidades.armas).length-1])+1
    for(var i = 0; i < Object.keys(probabilidades.armas).length; i++){
        if(probabilidades.armas[i+1] != undefined){
            if(probabilidades.armas[i] <= probArmas && probabilidades.armas[i+1] > probArmas ){
                talvezArma = armas[i]
            }
        }else{
            if(probabilidades.armas[i] <= probArmas){
                talvezArma = armas[i]
            }
        }
    }
    
    
    
    if(talvezAssassino != resposta.assassino){
        erros.push(1)
    }
    else if(talvezLocal != resposta.local){
        erros.push(2)
    }
    else if(talvezArma != resposta.arma){
        erros.push(3)
    }else{
        nacho = false
        console.log("Achou")
        console.log('Assassino:',talvezAssassino)
        console.log('Arma: ',talvezArma)
        console.log('Local: ',talvezLocal)
        console.log("Resposta:",resposta)
    }
    
    const Ajustador = 0.01
    var erroMostrado = erros[Math.floor(erros.length*Math.random())]
    //PENALIZAR
    if(erroMostrado == 1){
        for(var i = 0; i < suspeitos.length; i++){
            if(suspeitos[i] == talvezAssassino){
                diminuirProbabilidade(i,Ajustador,'suspeitos')
            }
        }
    }
    if(erroMostrado == 2){
        for(var i = 0; i < locais.length; i++){
            if(locais[i] == talvezLocal){
                diminuirProbabilidade(i,Ajustador,'locais')
            }
        }
    }
    if(erroMostrado == 3){
        for(var i = 0; i < armas.length; i++){
            if(armas[i] == talvezArma){
                diminuirProbabilidade(i,Ajustador,'armas')
            }
        }
    }
    
    
    
    //RECOMPENSAR
    if(erroMostrado != 1){
        for(var i = 0; i < suspeitos.length; i++){
            if(suspeitos[i] == talvezAssassino){
                aumentarProbabilidade(i,Ajustador,'suspeitos')
            }
        }
    }
    if(erroMostrado != 2){
        for(var i = 0; i < locais.length; i++){
            if(locais[i] == talvezLocal){
                aumentarProbabilidade(i,Ajustador,'locais')
            }
        }
    }
    if(erroMostrado != 3){
        for(var i = 0; i < armas.length; i++){
            if(armas[i] == talvezArma){
                aumentarProbabilidade(i,Ajustador,'armas')
            }
        }
    }
}
