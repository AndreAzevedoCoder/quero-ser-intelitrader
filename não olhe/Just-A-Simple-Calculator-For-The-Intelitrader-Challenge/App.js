import React from 'react';
import { StyleSheet, Text, View, TextInput,Picker } from 'react-native';
import { Button } from 'react-native-elements'

export default function App() {
  var [numero1valor, onChangeText1] = React.useState('');
  var [numero2valor, onChangeText2] = React.useState('');
  var [operacao, onChangeOperacao] = React.useState('');
  var resposta = 0

  var buttonColor
  const styles = StyleSheet.create({
    ViewPrincipal: {
      flex: 1,
      justifyContent: 'center',
      alignItems: "center",
    },
  });
  if(numero2valor != '' && numero1valor != ''){
    buttonColor = '#000'
  }else{
    buttonColor = '#DDD'
  }
  calcular = () => {
    if(numero2valor != '' && numero1valor != ''){
      switch(operacao){
        case "+":
          alert(parseInt(numero1valor) + parseInt(numero2valor))
          
          break;
        case "-":
          //alert(numero1valor - numero2valor)
          resposta = numero1valor - numero2valor
          alert(resposta)
          break;
        case "*":
          alert(numero1valor * numero2valor)
          break;
        case "/":
          if(numero2valor == 0){
            alert("Divisões por 0 são impossiveis")
          }else{
            alert(numero1valor / numero2valor)
          }
          break;
        default:
          alert(parseInt(numero1valor) + parseInt(numero2valor))
          break;
      }
      onChangeText1(0)
      onChangeText2(0)
    }
  }
  
  return (
    <View style={styles.ViewPrincipal}>

        <TextInput
          keyboardType = 'numeric'
          autoFocus = {true}
          style={{ height: 64,width: 240, borderColor: 'gray', borderWidth: 1 }}
          onChangeText={text => onChangeText1(text)}
          value={numero1valor}
        />

        <Picker
          selectedValue={operacao}
          style={{height: 50, width: 73, justifyContent: 'center'}}
          onValueChange={itemValue => onChangeOperacao(itemValue)
          }>
          <Picker.Item label="+" value="+" />
          <Picker.Item label="-" value="-" />
          <Picker.Item label="*" value="*" />
          <Picker.Item label="/" value="/" />
        </Picker>

        <TextInput
          keyboardType = 'numeric'
          style={{ height: 64,width: 240, borderColor: 'gray', borderWidth: 1 }}
          onChangeText={text => onChangeText2(text)}
          value={numero2valor}
        />



        <Button
          type={'outline'}
          title={'='}
          onPress={() => calcular()}           
          buttonStyle={{margin: 10,width: 50, borderColor: '#DDD', borderRadius: 20, padding: 13 }}
          titleStyle={{color: buttonColor }}
        />
  </View>
  );
  
};
